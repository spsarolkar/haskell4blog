<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
		Remove this if you use the .htaccess -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>index</title>
		<meta name="description" content="" />
		<meta name="author" content="Sunil Sarolkar" />
		<meta name="viewport" content="width=device-width; initial-scale=1.0" />
		<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
		<link rel="shortcut icon" href="/favicon.ico" />
		<link rel="apple-touch-icon" href="/apple-touch-icon.png" />
		<link rel="stylesheet" type="text/css" href="mainpage.css" title="Basic Style" media="all" />
		<script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?autoload=true&amp;skin=sunburst&amp;lang=css" defer="defer"></script>
		
	</head>
	<body>
		<div class="container">
		<div id="main">
	<div id="sidebar">
		<h4>Archives</h4>
		<ul class="archive-tree">
			<?cs each:x=archives ?>
			<li>
				<?cs name:x ?>
				<ul>
					<?cs each:y=x ?>
					<li><?cs var:y ?></li>
					<?cs /each ?>
				</ul>
			</li>
			<?cs /each ?>
		</ul>
	</div>

	<div id="content">
		<h3><?cs var:blog.title ?></h3>
		<var class="datelog"><?cs var:blog.date ?></var>
		<p><?cs var:blog.desc ?>
		</p>
		<p class="credits">tags: java, executables</p>
	</div>
	<hr class="clear" />
	</div>
	
	<div class="footer">
		<div class="footer-right">powered by haskell4web</div>
	<div class="footer-left">&copy; 2013 Sunil Sarolkar</div>
	<div class="footer-center">
	<ul>
			<li><a href="#">blog</a></li>
			<li><a href="https://docs.google.com/document/d/1hpyUD3L5NTE9ZDXqXYViTdL1eiDGs6grQ8p0ATbSucw/edit?pli=1">cv</a></li>
			<!--<li>projects</li>
			<li><a href="#">contact</a></li>-->
			<li>find me on <a href="http://stackoverflow.com/users/138604/xinus">Stack Overflow</a></li>
		</ul>
		</div>
	</div>
	</div>
	</body>
</html>
