module Main
where

import ServicesDao

main = do
	s<-getStreamingServicePort "HUNS" "stream"
	putStrLn $ show s
