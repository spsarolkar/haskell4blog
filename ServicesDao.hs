module ServicesDao
where

import Database.SednaTypes
import Database.SednaBindings
import Prelude hiding (catch)
import Control.Exception
import Database.SednaBindings
import Database.Internal.SednaConnectionAttributes
import Text.XML.HXT.Core
import Data.Text.Internal
import qualified Data.Text as T
import Data.Text
import Database.SednaExceptions
import Database.Internal.SednaConnectionAttributes

dbconnect = do
        let url="localhost"
        let dbname="services"
        let login="SYSTEM"
        let password="MANAGER"
        sednaConnect url dbname login password

getStreamingServicePort symbol typ = do
				sednaCon <- dbconnect
				catch (sednaBegin sednaCon) (\(e::SednaException) -> putStrLn $ show e)
				let query = "let $t:=doc(\"services\")/services/service[@name=\""++symbol++"\" and @type=\""++typ++"\"]/@port return data($t)"
				--let query = "let $t:=current-date() return data(doc(\"services\")/services/service[@name=\""++symbol++"\" and @type=\""++typ++"\" and updated=\"{$t}\"]/@port)"
				putStrLn $ query
				catch (sednaExecute sednaCon (query)) (\(e::SednaException) -> putStrLn $ ((show e) ++ ("-----Error sednaExecute")))
				queryResult <- sednaGetResult sednaCon
				let res = unpack queryResult
				putStrLn $ "res:" ++ res
				catch (sednaCommit sednaCon) (\(e::SednaException) -> putStrLn $ "Error sednaCommit")
				catch (sednaCloseConnection sednaCon) (\(e::SednaException) -> putStrLn $ "Error sednaCloseConnection")
				return res
