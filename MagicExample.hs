import Magic
import System.Environment (getArgs)

main =  do
  magic <- magicOpen [MagicMime]
  (file:_) <- getArgs
  magicLoadDefault magic
  mime <- magicFile magic file
  putStrLn mime
