module Session
	where

updateVal (name,val) [] = (name,val):[]
updateVal (name,val) ((i,v):ax) = if i==name then (name,val):ax else (i,v):updateVal (name,val) ax
