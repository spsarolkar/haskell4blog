module BlogDao
where

import Database.SednaTypes
import Database.SednaBindings
import Prelude hiding (catch)
import Control.Exception
import Database.SednaBindings
import Database.Internal.SednaConnectionAttributes
import Text.XML.HXT.Core
import Data.Text.Internal
import qualified Data.Text as T
--import Data.ByteString.Char8   (pack, unpack)
import Data.Text
import Database.SednaExceptions
import Database.Internal.SednaConnectionAttributes

dbconnect = do
	let url="localhost"
	let dbname="blog"
	let login="SYSTEM"
	let password="MANAGER"
	sednaConnect url dbname login password

data Blog = Blog String String String String
		deriving (Show)


data Comment = Comment String String String String String
		deriving (Show)

add :: Blog -> IO Bool
add (Blog id title desc date) = do
	sednaCon <- dbconnect
	putStrLn "connected"
	catch (sednaBegin sednaCon) (\(e::SednaException) -> putStrLn $ show e)
	if id /= "-1" then  
			do
			putStrLn "Updating old"
			let disableOldQuery = "UPDATE REPLACE $entry in doc('blog')/entries/entry[@id=\""++ id ++"\" and @active=1]/@active with attribute {\"active\"} { 0 }"
			catch (sednaExecute sednaCon (disableOldQuery)) (\(e::SednaException) -> putStrLn $ ((show e) ++ ("-----Error sednaExecute"))) 
			let insertQuery = "UPDATE "
					++"insert <entry id=\""++ id ++"\" active=\"1\" >"
					++"<title>"++title++"</title>"
					++"<description><![CDATA["++desc++"]]></description>"
					++"<date>{current-dateTime()}</date>"
					++"</entry>"
					++" into doc(\"blog\")/entries"
			catch (sednaExecute sednaCon (insertQuery)) (\(e::SednaException) -> putStrLn $ ((show e) ++ ("-----Error sednaExecute"))) 
			else 
			do
			let insertQuery = "UPDATE "
					++"insert <entry id=\"{ doc('blog')/entries/@count }\" active=\"1\" >"
					++"<title>"++title++"</title>"
					++"<description><![CDATA["++desc++"]]></description>"
					++"<date>{current-dateTime()}</date>"
					++"<comments></comments></entry>"
					++" into doc(\"blog\")/entries"
			catch (sednaExecute sednaCon (insertQuery)) (\(e::SednaException) -> putStrLn $ ((show e) ++ ("-----Error sednaExecute"))) 
			let updateCountQuery = "UPDATE REPLACE $count in doc('blog')/entries/@count with attribute {\"count\"} { $count + 1 }"
			catch (sednaExecute sednaCon (updateCountQuery)) (\(e::SednaException) -> putStrLn $ ((show e) ++ ("-----Error sednaExecute"))) 
			putStrLn "inserting new"
	catch (sednaCommit sednaCon) (\(e::SednaException) -> putStrLn $ "Error sednaCommit")
	catch (sednaCloseConnection sednaCon) (\(e::SednaException) -> putStrLn $ "Error sednaCloseConnection")
	putStrLn "disconnected"
	putStrLn "done"
	return True

update :: Blog -> IO Bool
update (Blog id title desc date) = do
	if id /= "-1" then  
		do
		sednaCon <- dbconnect
		putStrLn "connected"
		catch (sednaBegin sednaCon) (\(e::SednaException) -> putStrLn $ show e)
		let getCommentsQuery="doc('blog')/entries/entry[@id=\""++id++"\" and @active=\"1\"]/comments"
		catch (sednaExecute sednaCon (getCommentsQuery)) (\(e::SednaException) -> putStrLn $ ((show e) ++ ("-----Error sednaExecute"))) 
		queryResult <- sednaGetResult sednaCon
		let disableOldQuery = "UPDATE REPLACE $entry in doc('blog')/entries/entry[@id=\""++id++"\" and @active=\"1\"]/@active with attribute {\"active\"} { 0 }"
		putStrLn "Updating old"
		catch (sednaExecute sednaCon (disableOldQuery)) (\(e::SednaException) -> putStrLn $ ((show e) ++ ("-----Error sednaExecute"))) 
		putStrLn "inserting new"
		let insertQuery = "UPDATE "
                                ++"insert <entry id=\""++id++"\" active=\"1\">"
                                ++"<title>"++title++"</title>"
                                ++"<description><![CDATA["++desc++"]]></description>"
                                ++"<date>{current-dateTime()}</date>"
                                ++(unpack queryResult)++"</entry>"
                                ++" into doc(\"blog\")/entries"
		catch (sednaExecute sednaCon (insertQuery)) (\(e::SednaException) -> putStrLn $ ((show e) ++ ("-----Error sednaExecute"))) 
		catch (sednaCommit sednaCon) (\(e::SednaException) -> putStrLn $ "Error sednaCommit")
		catch (sednaCloseConnection sednaCon) (\(e::SednaException) -> putStrLn $ "Error sednaCloseConnection")
		putStrLn "disconnected"
		putStrLn "done"
		return True
	else
		do
		putStrLn $ ("Could not locate the blog with id-"++id)
		return False
	--let updateCountQuery = "UPDATE REPLACE $count in doc('blog')/entries/@count with attribute {\"count\"} { $count + 1 }"
	--catch (sednaExecute sednaCon (updateCountQuery)) (\(e::SednaException) -> putStrLn $ ((show e) ++ ("-----Error sednaExecute"))) 

getAll::IO [Blog]
getAll =do
	sednaCon <- dbconnect
	putStrLn "connected"
	catch (sednaBegin sednaCon) (\(e::SednaException) -> putStrLn $ show e)
	putStrLn "running query"
	sednaExecute sednaCon "let $d:=doc(\"blog\")/entries/entry[@active=\"1\"] \n return $d"
	queryResult <- sednaGetResult sednaCon
	putStrLn ("Query Result:" ++ (unpack queryResult))
	let doc = readString [] ("<entries>" ++ (unpack queryResult) ++ "</entries>")
	--blog <- runX $ doc /> hasName "entry" >>> ((getAttrValue "id") &&& (this /> hasName "title" /> getText) &&& ( this /> hasName "description" /> getText) &&& (this /> hasName "date" /> getText))
	blog <- runX $ (doc) /> hasName "entries" /> hasName "entry" >>> ((getAttrValue "id") &&& (this /> hasName "title" /> getText) &&& ( this /> hasName "description" /> getText) &&& (this /> hasName "date" /> getText))
	putStrLn $ "Blogs Parsed:" ++ show blog
	catch (sednaCommit sednaCon) (\(e::SednaException) -> putStrLn $ "Error sednaCommit")
	catch (sednaCloseConnection sednaCon) (\(e::SednaException) -> putStrLn $ "Error sednaCloseConnection")
	putStrLn "disconnected"
	putStrLn "done"
	return (fromArray blog)

--get::String -> IO Blog
get id =do
	sednaCon <- dbconnect
	putStrLn "connected"
	catch (sednaBegin sednaCon) (\(e::SednaException) -> putStrLn $ show e)
	putStrLn "running query"
	sednaExecute sednaCon ("let $d:=doc(\"blog\")/entries/entry[@active=\"1\" and @id=\""++id++"\"] \n return $d")
	queryResult <- sednaGetResult sednaCon
	putStrLn ("Query Result:" ++ (unpack queryResult))
	let doc = readString [] ("<entries>" ++ (unpack queryResult) ++ "</entries>")
	--blog <- runX $ doc /> hasName "entry" >>> ((getAttrValue "id") &&& (this /> hasName "title" /> getText) &&& ( this /> hasName "description" /> getText) &&& (this /> hasName "date" /> getText))
	blog <- runX $ (doc) /> hasName "entries" /> hasName "entry" >>> ((getAttrValue "id") &&& (this /> hasName "title" /> getText) &&& ( this /> hasName "description" /> getText) &&& (this /> hasName "date" /> getText))
	comments <- runX $ (doc) />hasName "entries" /> hasName "entry" /> hasName "comments" /> hasName "comment" >>> ((getAttrValue "id") &&& (this /> hasName "date" /> getText) &&& (this /> hasName "name" /> getText) &&& (this /> hasName "email" /> getText) &&& (this /> hasName "desc" /> getText))
	--comments <- runX $ (doc) />hasName "entries" /> hasName "entry" /> hasName "comments" /> hasName "comment" >>> ((this /> hasName "date" /> getText) &&& (this /> hasName "name" /> getText) &&& (this /> hasName "email" /> getText) &&& (this /> hasName "desc" /> getText))
	putStrLn $ "Blogs Parsed:" ++ show blog
	putStrLn $ "Comments parsed:" ++ (show comments)
	catch (sednaCommit sednaCon) (\(e::SednaException) -> putStrLn $ "Error sednaCommit")
	catch (sednaCloseConnection sednaCon) (\(e::SednaException) -> putStrLn $ "Error sednaCloseConnection")
	putStrLn "disconnected"
	putStrLn "done"
	let blg = (toBlog blog)
	let commentz = (toComments comments)
	return (blg, commentz)
	--return (toBlog blog)

latest =do
	sednaCon <- dbconnect
	putStrLn "connected"
	catch (sednaBegin sednaCon) (\(e::SednaException) -> putStrLn $ show e)
	putStrLn "running query"
	sednaExecute sednaCon ("let $b := doc(\"blog\")/entries/entry[@active=\"1\"]"
				++" let $mx := max( $b/@id )"
				++" return doc(\"blog\")/entries/entry[@id=$mx and @active=\"1\"]")
	queryResult <- sednaGetResult sednaCon
	putStrLn ("Query Result:" ++ (unpack queryResult))
	let doc = readString [] ("<entries>" ++ (unpack queryResult) ++ "</entries>")
	--blog <- runX $ doc /> hasName "entry" >>> ((getAttrValue "id") &&& (this /> hasName "title" /> getText) &&& ( this /> hasName "description" /> getText) &&& (this /> hasName "date" /> getText))
	blog <- runX $ (doc) /> hasName "entries" /> hasName "entry" >>> ((getAttrValue "id") &&& (this /> hasName "title" /> getText) &&& ( this /> hasName "description" /> getText) &&& (this /> hasName "date" /> getText))
	comments <- runX $ (doc) />hasName "entries" /> hasName "entry" /> hasName "comments" /> hasName "comment" >>> ((getAttrValue "id") &&& (this /> hasName "date" /> getText) &&& (this /> hasName "name" /> getText) &&& (this /> hasName "email" /> getText) &&& (this /> hasName "desc" /> getText))
	putStrLn $ "Blogs Parsed:" ++ show blog
	catch (sednaCommit sednaCon) (\(e::SednaException) -> putStrLn $ "Error sednaCommit")
	catch (sednaCloseConnection sednaCon) (\(e::SednaException) -> putStrLn $ "Error sednaCloseConnection")
	putStrLn "disconnected"
	putStrLn "done"
	let blg = (toBlog blog)
	let commentz = (toComments comments)
	return (blg, commentz)
	--return (toBlog blog)

getArchive::IO [(String,[(String,String)])]
getArchive =do
	sednaCon <- dbconnect
	putStrLn "connected"
	catch (sednaBegin sednaCon) (\(e::SednaException) -> putStrLn $ show e)
	putStrLn "running query"
	sednaExecute sednaCon ("for $entry in fn:doc(\"blog\")/entries//entry[@active=\"1\"] order by year-from-dateTime($entry/date/text()) "
				++ "return "
				++ " <entry id=\"{ $entry/@id }\">"
				++" { $entry/title }"
				++ " <year>{ year-from-dateTime(xs:dateTime($entry/date/text())) }</year></entry>")
	queryResult <- sednaGetResult sednaCon
	putStrLn ("Query Result:" ++ (unpack queryResult))
	let doc = readString [] ("<entries>" ++ (unpack queryResult) ++ "</entries>")
        --blog <- runX $ doc /> hasName "entry" >>> ((getAttrValue "id") &&& (this /> hasName "title" /> getText) &&& ( this /> hasName "description" /> getText) &&& (this /> hasName "date" /> getText))
	archives <- runX $ (doc) /> hasName "entries" /> hasName "entry" >>> ((getAttrValue "id") &&& (this /> hasName "title" /> getText) &&& ( this /> hasName "year" /> getText))
	putStrLn $ "Archives Parsed:" ++ show archives
	catch (sednaCommit sednaCon) (\(e::SednaException) -> putStrLn $ "Error sednaCommit")
	catch (sednaCloseConnection sednaCon) (\(e::SednaException) -> putStrLn $ "Error sednaCloseConnection")
	putStrLn "disconnected"
	putStrLn "done"
	return (fromArchiveArray archives)

addComment blog_id name email desc = do
					let exists=("fn:exists(doc(\"blog\")/entries/entry[@active=\"1\" and @id=\"" ++ blog_id ++ "\"]/comments)")
					sednaCon <- dbconnect
					putStrLn "connected"
					catch (sednaBegin sednaCon) (\(e::SednaException) -> putStrLn $ show e)
					putStrLn "running query"
					sednaExecute sednaCon (exists)
					queryResult <- sednaGetResult sednaCon
					putStrLn ("Query Result:" ++ (unpack queryResult))
					let ispresent=(unpack queryResult)
					let updateQuery = if ispresent=="true" then ("UPDATE insert <comment id=\"{ doc(\"blog\")/entries/entry[@active=\"1\" and @id=\""++ blog_id ++"\"]/comments/@count  }\" active=\"1\"><date>{current-dateTime()}</date><name>"++name++"</name><email>"++email++"</email><desc>"++ desc ++"</desc></comment> into doc(\"blog\")/entries/entry[@active=\"1\" and @id=\""++ blog_id ++"\"]/comments") else ("UPDATE insert <comments count=\"1\"><comment id=\"0\" active=\"1\"><date>{current-dateTime()}</date><name>"++name++"</name><email>"++email++"</email><desc>"++desc++"</desc></comment></comments> into doc(\"blog\")/entries/entry[@active=\"1\" and @id=\""++blog_id++"\"]")
					putStrLn updateQuery
					catch (sednaExecute sednaCon (updateQuery)) (\(e::SednaException) -> putStrLn $ "Error sedna")
					let updateCnt="UPDATE replace $count in doc(\"blog\")/entries/entry[@active=\"1\" and @id=\""++ blog_id ++"\"]/comments/@count with attribute {\"count\"} { $count + 1 }"
					putStrLn updateCnt
					catch (sednaExecute sednaCon (updateCnt)) (\(e::SednaException) -> putStrLn $ "Error sedna")
					catch (sednaCommit sednaCon) (\(e::SednaException) -> putStrLn $ "Error sednaCommit")
					catch (sednaCloseConnection sednaCon) (\(e::SednaException) -> putStrLn $ "Error sednaCloseConnection")
					return True

					

fromArchiveArray [] = []
fromArchiveArray l@((id,(ax,ay)):cz) = (ay,combined) : fromArchiveArray rest
				where
					combine _ [] = ([],[])
					combine year l@((id,(ttl,yr)):xs) = if year == yr then let res = (combine year xs) in ((id,ttl):(fst res),(snd res)) else ([],l)
					(combined,rest) = combine ay l

toBlog ((id,(title,(desc,date))):_) = (Blog id title desc date)

fromArray [] = []
fromArray ((id,(title,(desc,date))):xs) = (Blog id title desc date) : fromArray xs

toComments [] = []
toComments ((id,(date,(name,(email,desc)))):ax) = (Comment id date name email desc):toComments ax
