module Tradeware
	where
import Constants
import ServicesDao
import Data.List.Split

execute ::[(String,String)]-> [(String,String)] -> IO (String,[(String,String)],[(String,String)])
execute params sdata= do
		let symbol=getVal params "code"
		putStrLn $"Symbol:"++symbol
		s<-getStreamingServicePort symbol "stream"	
		return ("SUCCESS",[("code",symbol),("StreamingPort",s)],sdata)

getVal (_:((url,_):_)) name = findParam (splitOn "/" url)
                                where
                                findParam [] =""
                                findParam (p:[]) = ""
                                findParam (p:rest@(x:xs)) = if p==name then x else findParam rest

param::[(String,String)] -> String -> String
param [] _ = ""
param ((x,y):rest) name = if x == name then y else (param rest name)
