module Blog
	where

import Data.List.Split
import BlogDao
import Data.Time
import Data.Time.Format
import System.Locale
import Text.Blaze.Html.Renderer.Text
import Text.Markdown

execute ::[(String,String)]->[(String,String)]-> IO (String,[(String,String)],[(String,String)])
execute params sdata= do
		archives <- getArchive
		blogs <- getAll
		let blogList = toHdf blogs
		let archiveLst = hdfArchive archives
		putStrLn ("**" ++archiveLst)
		return ("SUCCESS",[("ip",(show params)),("",blogList),("",archiveLst)],sdata)

new ::[(String,String)] ->[(String,String)]->IO (String,[(String,String)],[(String,String)])
new params sdata = do
		return ("SUCCESS",[],[])

edit ::[(String,String)]->[(String,String)]-> IO (String,[(String,String)],[(String,String)])
edit params sdata= do
		let id=getVal params "edit"
		putStrLn ("opening blog for editing ,id="++id)
		let lid=(param sdata "login")
		if lid=="" then 
			do
			return ("UNAUTHORISED",[("errormessage","Please login to use this feature")],[])
		else
			if id/="" then
				do 
				(blog,comments)<- BlogDao.get id
				let hdfBlog=(blogToHdf blog)
				return ("SUCCESS",[("ip",(show params)),("",hdfBlog)],[])
			else
				do return ("ERROR",[("errormessage","id is null")],[])
			

add ::[(String,String)]-> [(String,String)] -> IO (String,[(String,String)],[(String,String)])
add params sdata= do
		ct <- getCurrentTime
		putStrLn "reading request param"
		let lid=(param sdata "login")
		let title = param params "title"
		let desc = param params "desc"
		putStrLn $ "user already logged in, login:"++lid
		if lid /= "" then
			do
			putStrLn $ show params
			let blogTobeAdded = (Blog "-1" (param params "title") (param params "desc") "")
			putStrLn (show blogTobeAdded)
			BlogDao.add blogTobeAdded  --(Blog (param params "title") (param params "desc") (show ct))
			putStrLn "Record added"
			return ("SUCCESS",[],sdata)
		else
			do
			putStrLn "Request errored"
			return ("ERROR",[("title",title),("desc",desc),("message","Invalid password")],sdata)

update ::[(String,String)]-> [(String,String)] -> IO (String,[(String,String)],[(String,String)])
update params sdata= do
		ct <- getCurrentTime
		putStrLn "reading request param"
		let lid=(param sdata "login")
		let title = param params "title"
		let desc = param params "desc"
		let blog_id = param params "blog_id"
		putStrLn $ "user already logged in, login:"++lid
		if ((lid /= "") && (blog_id/="")) then
			do
			putStrLn $ show params
			let blogTobeUpdated = (Blog blog_id title desc "")
			putStrLn (show blogTobeUpdated)
			BlogDao.update blogTobeUpdated  --(Blog (param params "title") (param params "desc") (show ct))
			putStrLn "Record updated"
			return ("SUCCESS",[("message","Blog updated succesfully")],sdata)
		else
			do
			putStrLn "Request errored"
			return ("ERROR",[("title",title),("desc",desc),("message","Invalid password")],sdata)

param::[(String,String)] -> String -> String
param [] _ = ""
param ((x,y):rest) name = if x == name then y else (param rest name)


get ::[(String,String)]->[(String,String)]-> IO (String,[(String,String)],[(String,String)])
get params sdata= do
		let id=getVal params "get"
		putStrLn ("id="++id)
		archives <- getArchive
		let archiveLst = hdfArchive archives
		--return ("SUCCESS",[("ip",(show params))])
		--let sdata=("login","Guest"):sdata
		if id/="" then
			do 
			(blog,comments)<- BlogDao.get id
			let hdfBlog=(blogToHdf blog)
			let commentsHdf = (hdfComments comments)
			putStrLn ("Commentshdf:"++commentsHdf)
			return ("SUCCESS",[("ip",(show params)),("",hdfBlog),("",commentsHdf),("",archiveLst)],(("login","Guest"):sdata))
		else return ("ERROR",[("message","requested entry not found")],sdata)
		
latest ::[(String,String)]-> [(String,String)] -> IO (String,[(String,String)],[(String,String)])
latest params sdata= do
			archives <- getArchive
			let archiveLst = hdfArchive archives
			(blog,comments)<- BlogDao.latest
			let hdfBlog=(blogToHdf blog)
			return ("SUCCESS",[("ip",(show params)),("",hdfBlog),("",archiveLst)],sdata)

addComment:: [(String,String)]-> [(String,String)] -> IO (String,[(String,String)],[(String,String)])
addComment params sdata= do
			let blog_id=(param params "blog_id")
			let name = (param params "name")
			let email = (param params "email")
			let desc = (param params "desc")
			res<-(BlogDao.addComment blog_id  name email desc)
			if res == True then return ("SUCCESS",[("msg","Comment added succesfully"),("blog_id",blog_id)],updateVal ("name",name) (updateVal ("email",email) sdata))
			else return ("ERROR",[("errMsg","Error while adding Comment")],updateVal ("name",name) (updateVal ("email",email) sdata))

updateVal (name,val) [] = (name,val):[]
updateVal (name,val) ((i,v):ax) = if i==name then (name,val):ax else (i,v):updateVal (name,val) ax
		
getVal (_:((url,_):_)) name = findParam (splitOn "/" url)
				where
				findParam [] =""
				findParam (p:[]) = ""
				findParam (p:rest@(x:xs)) = if p==name then x else findParam rest


hdfComments::[Comment]->String
hdfComments [] = "comments{\n}"
hdfComments l@(c:cx) = "comments{"++(hdfComments' l 0) ++"\n}"

hdfComments' [] _= ""
hdfComments' l@((Comment id date name email desc):cx) cnt = "\n"++(show cnt)++"{\n"
				++ "id=" ++ id ++ "\n"
				++ "name=" ++ name ++ "\n"
				++ "email=" ++ email ++ "\n"
				++ "desc << EOM\n" ++ desc ++ "\nEOM"
				++ "\ndate="++date++"\n}"++(hdfComments' cx (cnt+1))

toHdf::[Blog]->String
toHdf l@(b:bx) = "blogs{" ++ (toHdf' l 0) ++ "\n}"

toHdf' [] _ = ""
toHdf' l@((Blog id title desc date):bx) cnt = "\n"++(show cnt) ++ "{\n"
				++ "id="++id++"\n"
				++ "title="++title++"\n"
				++ "desc << EOM\n"++ desc ++"\nEOM"
				++ "\ndate="++date++"\n}"++ (toHdf' bx (cnt+1))

blogToHdf blog = "blog" ++ (blogToHdf' blog) 

blogToHdf' (Blog id title desc date) = "{\n"
				++ "id="++id++"\n"
				++ "title="++title++"\n"
				++ "desc << EOM\n"++ desc++"\nEOM"
				++ "\ndate="++date++"\n}"

hdfArchive l = "archives{"++(archiveToHdf l)++"\n}";


archiveToHdf [] = ""
archiveToHdf ((y,l):yx) = "\n"++y++"{"++ (archiveToHdf' 0 l) ++ "\n}" ++ archiveToHdf yx
			where
			archiveToHdf' _ [] = ""
			archiveToHdf' cnt (x:xs) = "\n" ++ (show cnt) ++ "{\ntitle=" ++ (snd x)++"\nid="++(fst x) ++ "\n}"++ (archiveToHdf' (cnt+1) xs)
