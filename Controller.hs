module Controller
where
import View
import Home
import User
import User
import Blog
import Blog
import Blog
import Blog
import Blog
import Blog
import Blog
import Tradeware
import Home
import Utils
request url params sdata=
			if Utils.match url "*.css" then return ("RESOURCE",[]) 
			else if Utils.match url "js/*" then return ("RESOURCE",[]) 
			else if Utils.match url "images/*" then return ("RESOURCE",[]) 
			else if Utils.match url "css/*.css" then return ("RESOURCE",[]) 
			else if Utils.match url "home" then 
					do
					(res,params,sdata)<-Home.execute params sdata
					case res of
						"SUCCESS"->do
							out<-View.render "home/HomeSuccess.cs:home/HomeSuccess.hdf" params sdata
							return (out,sdata)
						"ERROR"->do
							out<-View.render "home/HomeError.cs:home/homeError.hdf" params sdata
							return (out,sdata)
			else if Utils.match url "user/login" then 
					do
					(res,params,sdata)<-User.login params sdata
					case res of
						"SUCCESS"->do
							out<-View.render "user/LoginSuccess.cs:user/LoginSuccess.hdf" params sdata
							return (out,sdata)
						"ERROR"->do
							out<-View.render "user/LoginError.cs:user/LoginError.hdf" params sdata
							return (out,sdata)
			else if Utils.match url "user/validate" then 
					do
					(res,params,sdata)<-User.validate params sdata
					case res of
						"SUCCESS"->do
							out<-View.render "user/ValidateSuccess.cs:user/ValidateSuccess.hdf" params sdata
							return (out,sdata)
						"ERROR"->do
							out<-View.render "user/ValidateError.cs:user/ValidateError.hdf" params sdata
							return (out,sdata)
			else if Utils.match url "blog/get" then 
					do
					(res,params,sdata)<-Blog.get params sdata
					case res of
						"SUCCESS"->do
							out<-View.render "blog/BlogGetSuccess.cs:blog/BlogGetSuccess.hdf" params sdata
							return (out,sdata)
						"ERROR"->do
							out<-View.render "blog/BlogGetError.cs:blog/BlogGetError.hdf" params sdata
							return (out,sdata)
			else if Utils.match url "blog/new" then 
					do
					(res,params,sdata)<-Blog.new params sdata
					case res of
						"SUCCESS"->do
							out<-View.render "blog/BlogNewSuccess.cs:blog/BlogNewSuccess.hdf" params sdata
							return (out,sdata)
						"ERROR"->do
							out<-View.render "blog/BlogNewError.hsp" params sdata
							return (out,sdata)
			else if Utils.match url "blog/update" then 
					do
					(res,params,sdata)<-Blog.update params sdata
					case res of
						"SUCCESS"->do
							out<-View.render "blog/BlogUpdateSuccess.cs:blog/BlogUpdateSuccess.hdf" params sdata
							return (out,sdata)
						"ERROR"->do
							out<-View.render "blog/BlogUpdateError.cs:blog/BlogUpdateError.hsp" params sdata
							return (out,sdata)
			else if Utils.match url "blog/add" then 
					do
					(res,params,sdata)<-Blog.add params sdata
					case res of
						"SUCCESS"->do
							out<-View.render "blog/BlogAddSuccess.cs:blog/BlogAddSuccess.hdf" params sdata
							return (out,sdata)
						"ERROR"->do
							out<-View.render "blog/BlogNewSuccess.cs:blog/BlogNewSuccess.hdf" params sdata
							return (out,sdata)
			else if Utils.match url "blog/edit" then 
					do
					(res,params,sdata)<-Blog.edit params sdata
					case res of
						"SUCCESS"->do
							out<-View.render "blog/BlogEditSuccess.cs:blog/BlogEditSuccess.hdf" params sdata
							return (out,sdata)
						"ERROR"->do
							out<-View.render "blog/BlogEditSuccess.cs:blog/BlogEditSuccess.hdf" params sdata
							return (out,sdata)
						"UNAUTHORISED"->do
							out<-View.render "blog/BlogEditUnauthorised.cs:blog/BlogEditUnauthorised.hdf" params sdata
							return (out,sdata)
			else if Utils.match url "comment/add" then 
					do
					(res,params,sdata)<-Blog.addComment params sdata
					case res of
						"SUCCESS"->do
							out<-View.render "blog/CommentAddSuccess.cs:blog/CommentAddSuccess.hdf" params sdata
							return (out,sdata)
						"ERROR"->do
							out<-View.render "blog/CommentAddError.cs:blog/CommentNewError.hdf" params sdata
							return (out,sdata)
			else if Utils.match url "blog" then 
					do
					(res,params,sdata)<-Blog.execute params sdata
					case res of
						"SUCCESS"->do
							out<-View.render "blog/BlogSuccess.cs:blog/BlogSuccess.hdf" params sdata
							return (out,sdata)
						"ERROR"->do
							out<-View.render "blog/BlogError.cs:blog/BlogError.hdf" params sdata
							return (out,sdata)
			else if Utils.match url "tradeware" then 
					do
					(res,params,sdata)<-Tradeware.execute params sdata
					case res of
						"SUCCESS"->do
							out<-View.render "tradeware/TradewareSuccess.cs:tradeware/TradewareSuccess.hdf" params sdata
							return (out,sdata)
						"ERROR"->do
							out<-View.render "home/HomeError.cs:home/homeError.hdf" params sdata
							return (out,sdata)
			else if Utils.match url "*" then 
					do
					(res,params,sdata)<-Home.execute params sdata
					case res of
						"SUCCESS"->do
							out<-View.render "home/HomeSuccess.cs:home/HomeSuccess.hdf" params sdata
							return (out,sdata)
						"ERROR"->do
							out<-View.render "home/HomeError.cs:home/homeError.hdf" params sdata
							return (out,sdata)
			else return ("No Mapping Found",[])
