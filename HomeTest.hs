module Main
where

import Home

main = do
	op <- execute []
	--op <- add [("title","test title"),("desc","test desc")]
	--op <-get [("test.fcgi",""),("blog/get/1","")]
	--op <-latest [("test.fcgi",""),("home","")]
	putStrLn $ show $ op
