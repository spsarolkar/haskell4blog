module Session.Data.Sedna.Sedna
where

import Database.SednaTypes
import Database.SednaBindings
import Prelude hiding (catch)
import Control.Exception
import Database.SednaBindings
import Database.Internal.SednaConnectionAttributes
import Text.XML.HXT.Core
import Data.Text.Internal
import qualified Data.Text as T
--import Data.ByteString.Char8   (pack, unpack)
import Data.Text
import Database.SednaExceptions
import Database.Internal.SednaConnectionAttributes
--

dbconnect = do
        let url="localhost"
        let dbname="sessions"
        let login="SYSTEM"
        let password="MANAGER"
        sednaConnect url dbname login password


data Session = Session String [(String,String)]

allSessions :: IO [Session]
allSessions = do
		sednaCon <- dbconnect
		catch (sednaBegin sednaCon) (\(e::SednaException) -> putStrLn $ show e)
		let selectAll = "let $d:=doc(\"sessions\")/sessions/session[@active=\"1\"] \n return $d"
		putStrLn $ "Query:" ++ selectAll
		sednaExecute sednaCon selectAll
		queryResult <- sednaGetResult sednaCon
		putStrLn ("Query Result:" ++ (unpack queryResult))
		let doc = readString [] ("<sessions>" ++ (unpack queryResult) ++ "</sessions>")
		--sessions <- runX $ (doc) /> hasName "sessions" /> hasName "session" >>> ((getAttrValue "id") &&& ((this /> hasName "map") >>> (( this /> hasName "key" /> getText) &&& (this /> hasName "val" /> getText))))
		sessions <- runX $ (doc) /> hasName "sessions" /> hasName "session" >>> ((getAttrValue "id") &&& (this/> hasName "maps" /> hasName "map" >>> ((this /> hasName "key" /> getText) &&& (this /> hasName "value" /> getText))))
		let sessionsObjList = getSessionsMap sessions
		printSessions sessionsObjList
		--putStrLn $ show sessionsObjList
		--let sessionObjs=sessionObj sessions
		--putStrLn $ show sessionObjs
		return sessionsObjList

printSessions [] = do
			putStrLn "End"
printSessions ((Session id param):x) = do
					putStrLn $ "id:"++id++",params:"++(show param)
					printSessions x

getSessionsMap [] = []
getSessionsMap sess@((id,(key,val)):rest) = (Session id combined) : getSessionsMap rest3
					where
						merge [] x = (x,[])
						merge l@((id2,(key2,val2)):rest2) res = if id == id2 then (merge rest2 $(key2,val2):res) else (res,l)

						(combined,rest3)=merge sess []

sessionParams :: String -> IO [(String,String)]
sessionParams sessionId = do
		sednaCon <- dbconnect
                catch (sednaBegin sednaCon) (\(e::SednaException) -> putStrLn $ show e)
                let selectAll = "let $d:=doc(\"sessions\")/sessions/session[@active=\"1\" and @id=\""++ sessionId ++"\"] \n return $d"
                putStrLn $ "Query:" ++ selectAll
                sednaExecute sednaCon selectAll
                queryResult <- sednaGetResult sednaCon
                putStrLn ("Query Result:" ++ (unpack queryResult))
                let doc = readString [] ("<sessions>" ++ (unpack queryResult) ++ "</sessions>")
                --sessions <- runX $ (doc) /> hasName "sessions" /> hasName "session" >>> ((getAttrValue "id") &&& ((this /> hasName "map") >>> (( this /> hasName "key" /> getText) &&& (this /> hasName "val" /> getText))))
                session <- runX $ (doc) /> hasName "sessions" /> hasName "session" >>> ((getAttrValue "id") &&& (this/> hasName "maps" /> hasName "map" >>> ((this /> hasName "key" /> getText) &&& (this /> hasName "value" /> getText))))
                let ((Session _ params):_) = getSessionsMap session
		catch (sednaCloseConnection sednaCon) (\(e::SednaException) -> putStrLn $ "Error sednaCloseConnection")
		return params

createSession :: IO Session
createSession = do
		sednaCon <- dbconnect
		catch (sednaBegin sednaCon) (\(e::SednaException) -> putStrLn $ show e)
		let selectCntQuery = "fn:doc(\"sessions\")/sessions/@cnt/string()"
		sednaExecute sednaCon selectCntQuery
		queryResult <- sednaGetResult sednaCon
		putStrLn ("Query Result:" ++ (unpack queryResult))
		let insertQuery = "UPDATE insert <session id=\"{ fn:doc('sessions')/sessions/@cnt }\" active=\"1\"><maps></maps></session> into fn:doc(\"sessions\")/sessions"
		putStrLn insertQuery
		catch (sednaExecute sednaCon insertQuery) (\(e::SednaException) -> putStrLn $ ((show e) ++ ("-----Error sednaExecute")))
		let updateCntQuery ="UPDATE REPLACE $cnt in doc('sessions')/sessions/@cnt with attribute {\"cnt\"} { $cnt + 1 }"
		putStrLn updateCntQuery
		catch (sednaExecute sednaCon updateCntQuery) (\(e::SednaException) -> putStrLn $ ((show e) ++ ("-----Error sednaExecute")))
		--queryResult <- sednaGetResult sednaCon
		--putStrLn ("Query Result:" ++ (unpack queryResult))
                catch (sednaCommit sednaCon) (\(e::SednaException) -> putStrLn $ "Error sednaCommit")
		catch (sednaCloseConnection sednaCon) (\(e::SednaException) -> putStrLn $ "Error sednaCloseConnection")
		return (Session (unpack queryResult) [])

--updateSession :: Session 
updateSession (Session id params) = do
					let maps = getMaps params
					let updateQuery = "UPDATE REPLACE $maps in doc('sessions')/sessions/session[@id=\""++ id ++"\"]/maps with <maps>"++ maps ++"</maps>"
					sednaCon <- dbconnect
					catch (sednaBegin sednaCon) (\(e::SednaException) -> putStrLn $ show e)
					putStrLn $ updateQuery
					catch (sednaExecute sednaCon updateQuery) (\(e::SednaException) -> putStrLn $ ((show e) ++ ("-----Error sednaExecute")))
					putStrLn "comitting.."
					catch (sednaCommit sednaCon) (\(e::SednaException) -> putStrLn $ "Error sednaCommit")
					catch (sednaCloseConnection sednaCon) (\(e::SednaException) -> putStrLn $ "Error sednaCloseConnection")

getMaps:: [(String,String)] -> String
getMaps [] = ""
getMaps ((x,y):l) = "<map><key>" ++ x ++ "</key><value>" ++ y ++ "</value></map>" ++ (getMaps l)

--sessionObj [] = []
--sessionObj ((id,(key,val)):rest) = (Session id (key,val)):(sessionObj rest)

--getSession :: String -> Session
--getSession sessionId = 
