import Control.Concurrent
import System.Posix.Process (getProcessID)
import Network.FastCGI
--import ActionController
import Controller
import Control.Monad.Trans
import Magic
import System.IO
import System.Directory
import qualified Data.ByteString.Lazy as B
import MimeMap
import Data.List.Split
import Data.List
import Constants
import Session.Data.Sedna.Sedna
import Control.Monad (liftM)
import Data.Maybe (fromMaybe)
import Data.List.Split
import Data.String.Utils

test :: CGI CGIResult
test = 
	do
--	setHeader "Content-type" "text/plain"
	pid <- liftIO getProcessID
	threadId <- liftIO myThreadId
--          query <- getInput "query"
	query <- pathInfo
	queryStr <- queryString
	reqMethod <- requestMethod
	params <- getInputs
	let url = drop ((length "test.ghci") + 1)  queryStr
	hdr <- liftM (fromMaybe "h4wsessionid=-1") (requestHeader "Cookie")
	let sid= head $! tail (splitOn "=" hdr)
	liftIO $ putStrLn ("sid=" ++ sid)
	let sess_id=sid
	sdata <- liftIO $ if sess_id /= "-1" then sessionParams sess_id else return []
	--let sdata=[]
	(viewStr,c) <- liftIO $ Controller.request (drop ((length "test.ghci") + 1)  queryStr) params sdata
	
	if not (null c) then 
			do
			if sess_id == "-1" then 
				do
				liftIO $ putStrLn "creating new session..."
				(Session id _) <- liftIO $ createSession 
				liftIO $ liftIO $ updateSession (Session id c)
				liftIO $ putStrLn ("session created :" ++ id)
				setCookie (Cookie "h4wsessionid" id Nothing Nothing (Just "/") False) 
			else	do
				liftIO $ putStrLn "updating existing session..."
				liftIO $ liftIO $ updateSession (Session sess_id c)
	else do return ()
	
	if viewStr == "RESOURCE" 
	then
		do
		let mime = getMime $ last (splitOn "." url) 
		if mime == "" then
			do
			contentType <- liftIO (getContentType ((web_root) ++ url))
			setHeader "Content-type" contentType
		else
			do
			setHeader "Content-type" mime
		b <- liftIO (B.readFile ((web_root) ++ url))
		outputFPS b
	else 
		do
		let mime = getMime $ last (splitOn "." url)
		if mime == "" then
			do
			  isExist <- liftIO $ doesFileExist ((web_root) ++ url)
			  liftIO $ putStrLn ("request received:"++url)
			  if isExist then
			  	do
			  	contentType <- liftIO (getContentType ((web_root) ++ url))
			  	setHeader "Content-type" contentType --"text/css"
			  	else
			  	do
			  	setHeader "Content-type" "text/html"
		else 
			do
			  setHeader "Content-type" mime
			
		let tid = concat $ drop 1 $ words $ show threadId
		liftIO $ putStrLn ("Called the text content type, url:")
		output $ viewStr
		--output $ unlines [ "Process ID: " ++ show pid, 
		--	"Thread ID:  " ++ tid,"query:" ++ show query , "viewstore:" ++ show viewStr , "queryStr=" ++ show (drop ((length "test.ghci") + 1)  queryStr) ]

getContentType path = do
			magic <- magicOpen [MagicMime]
			magicLoadDefault magic
			mime <- magicFile magic path
			return mime

main = runFastCGIConcurrent' forkIO 10 test

