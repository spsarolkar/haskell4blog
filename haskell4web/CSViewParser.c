/*
 * Copyright 2001-2004 Brandon Long
 * All Rights Reserved.
 *
 * ClearSilver Templating System
 *
 * This code is made available under the terms of the ClearSilver License.
 * http://www.clearsilver.net/license.hdf
 *
 */

#include "cs_config.h"
#include "prototypes.h"

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <malloc.h>
#include "util/neo_misc.h"
#include "util/neo_hdf.h"
#include "cs.h"
#include <stdlib.h>

/*static NEOERR *output (void *ctx, char *s)
{
  printf ("%s", s);
  return STATUS_OK;
}*/

NEOERR *test_strfunc(const char *str, char **ret)
{
  char *s = strdup(str);
  int x = 0;

  if (s == NULL)
    return nerr_raise(NERR_NOMEM, "Unable to duplicate string in test_strfunc");

  while (s[x]) {
    s[x] = tolower(s[x]);
    x++;
  }
  *ret = s;
  return STATUS_OK;
}

void log(char* text){
	//writeToFile("/var/www/localhost/view/render.log",text);
}

char* parseTemplate (char* hdf_file,char* cs_file)
{
//  log("Starting to parse");
  char* retVal=NULL;
  NEOERR *output (void *ctx, char *s)
  {
    //retVal=s;
    if(retVal==NULL){
	retVal = (char*) malloc (sizeof (char) * (strlen (s)) + 1 );
	retVal[0]='\0';
    }
    else {
//	char buffer[1000]="\0";
//	sprintf(buffer,">reallocating from %d to %d",strlen(retVal),strlen(retVal)+strlen(s));
//	log(buffer);
	retVal = (char*) realloc (retVal , sizeof (char) * (strlen(retVal) + strlen(s)) + 1);
    }
    strcat(retVal,s);
  //  printf ("Earlier: %s", s);
    return STATUS_OK;
  }

  NEOERR *err;
  CSPARSE *parse;
  HDF *hdf;
  int verbose = 0;
  //char *hdf_file, *cs_file;
  
/*  if (argc < 3)
  {
    ne_warn ("Usage: cstest [-v] <file.hdf> <file.cs>");
    return -1;
  }

  if (!strcmp(argv[1], "-v"))
  {
    verbose = 1;
    if (argc < 4)
    {
      ne_warn ("Usage: cstest [-v] <file.hdf> <file.cs>");
      return -1;
    }
    hdf_file = argv[2];
    cs_file = argv[3];
  }
  else
  {
    hdf_file = "/usr/src/web/ffi/test.hdf";
    cs_file = "/usr/src/web/ffi/test.cs";
//  }*/
//log("initialising hdf");
  err = hdf_init(&hdf);
  if (err != STATUS_OK)
  {
    nerr_log_error(err);
    return "-1";
  }
  //printf("\nhdf_file: %s",hdf_file);
  //writeToFile("/usr/src/web/myproj/temp/hdf_received_to_c.txt",hdf_file);
  //log("parsing hdf");
  err = hdf_read_string(hdf, hdf_file);
  //log("hdf parsing done");
  if (err != STATUS_OK)
  {
    //log("Error occured while parsing");
    nerr_log_error(err);
    return "-1";
  }

  //printf("\nParsing %s\n", cs_file);
  err = cs_init (&parse, hdf);
  if (err != STATUS_OK)
  {
    nerr_log_error(err);
    return "-1";
  }

  /* register a test strfunc */
  err = cs_register_strfunc(parse, "test_strfunc", test_strfunc);
  if (err != STATUS_OK) 
  {
    nerr_log_error(err);
    return "-1";
  }

  //writeToFile("/usr/src/web/myproj/temp/cs_received_to_c.txt",cs_file);
  //printf("\ncs_file: %s",cs_file);
  //log("starting to parse");
  err = cs_parse_string (parse, cs_file, strlen(cs_file));
  if (err != STATUS_OK)
  {
    err = nerr_pass(err);
    nerr_log_error(err);
    return "-1";
  }
//log("starting to render");
  err = cs_render(parse, NULL, output);
  if (err != STATUS_OK)
  {
    err = nerr_pass(err);
    nerr_log_error(err);
    return "-1";
  }

  if (verbose)
  {
    printf ("\n-----------------------\nCS DUMP\n");
    err = cs_dump(parse, NULL, output);
  }

  cs_destroy (&parse);

  if (verbose)
  {
    printf ("\n-----------------------\nHDF DUMP\n");
    hdf_dump (hdf, NULL);
  }
  hdf_destroy(&hdf);

  /*char* ret = (char*) malloc(10 * sizeof(char));
  ret[0] = 'a';
  ret[1] = 'a';
  ret[2] = 'a';
  ret[3] = 'a';
  ret[4] = 'a';
  ret[5] = 'a';
  */
  //printf ("now: %s", retVal);
  return retVal;
}

int writeToFile(char* filePath, char* contents){
      FILE *fp;
      int i;
   
      /* open the file */
      fp = fopen(filePath, "a");
      if (fp == NULL) {
         printf("I couldn't open results.dat for writing.\n");
         exit(0);
      }
   
      /* write to the file */
      //for (i=0; i<=10; ++i)
      fprintf(fp, "%s\n", contents);
   
      /* close the file */
      fclose(fp);
   
      return 0;
}
/*int main(){
	parseTemplate("/usr/src/web/ffi/test.hdf","/usr/src/web/ffi/test.cs");
	return 0;
}*/
