{-# LANGUAGE ForeignFunctionInterface #-}
module View
	where
import Data.List.Split
import Foreign.C
import Foreign.Ptr
import Foreign
import Foreign.Marshal.Alloc
import Constants
import System.IO.UTF8

web_home = web_root ++"/view/"

render::String -> [(String,String)] -> [(String,String)] -> IO String
render fileName info sparam= do 
			let vw=splitOn ":" fileName 
			--x <- (readFile ("/usr/src/web/" ++ (head vw))) --fileName
			hdfStr <- (System.IO.UTF8.readFile (web_home ++ (head (tail vw)))) -- "view/home/HomeSuccess.hdf"))
			--hdfStr <- (io.readFile (web_home ++ "view/test.hdf"))
			let hdfStr2 = hdfStr ++ "\n"++ (toHdf info) ++ (toTokenHdf sparam "session")
			System.IO.UTF8.putStrLn hdfStr2
			csStr <- (System.IO.UTF8.readFile (web_home ++ (head vw))) --"view/home/HomeSuccess.cs"))
			--csStr <- (io.readFile (web_home ++ "view/test.cs"))
			--System.IO.UTF8.putStrLn $"HDF:"++hdfStr2
			--System.IO.UTF8.putStrLn $"CS:"++csStr
			hdfFile <- newCString hdfStr2 --(web_home ++ "view/test.hdf")
			csFile <- newCString csStr --(web_home ++ "view/test.cs")
			str <- parseTemplate hdfFile csFile
          		ret <- peekCString str
			free str
			--free hdfFile
			--free csFile 
			return $ ret

toHdf [] = ""
toHdf ((key,val):xs) = if "" /= key then "\n" ++ key ++ "=" ++ val ++ (toHdf xs) else "\n" ++ val ++(toHdf xs)

toTokenHdf::[(String,String)] -> String -> String
toTokenHdf [] token = ""
toTokenHdf ((key,val):xs) token = if "" /= (token ++"_"++ key) then "\n" ++ (token ++ "_" ++ key) ++ "=" ++ val ++ (toTokenHdf xs token) else "\n" ++ val ++(toTokenHdf xs token)

foreign import ccall safe "prototypes.h"
	parseTemplate :: CString -> CString -> IO CString
