module MimeMap
where

mime = [("css","text/css")]

getMime ext = find ext mime

find _ [] = ""
find ext ((m_ext,m_mime):rest) = if m_ext == ext then m_mime else find ext rest
