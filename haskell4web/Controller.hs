module Controller
where
import View
import Home
import Utils
request url params sdata=
			 if Utils.match url "home" then 
					do
					(res,params,sdata)<-Home.execute params sdata
					case res of
						"SUCCESS"->do
							out<-View.render "home/HomeSuccess.cs:home/HomeSuccess.hdf" params sdata
							return (out,sdata)
						"ERROR"->do
							out<-View.render "home/HomeError.cs:home/HomeError.hdf" params sdata
							return (out,sdata)
			else if Utils.match url "*.css" then return ("RESOURCE",[]) 
			else if Utils.match url "images/*" then return ("RESOURCE",[]) 
			else return ("No Mapping Found",[])