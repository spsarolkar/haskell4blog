module Request
	where

param::[(String,String)] -> String -> String
param [] _ = ""
param ((x,y):rest) name = if x == name then y else (param rest name)
