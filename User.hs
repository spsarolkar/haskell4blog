module User
        where

import Session
import Request
import UserDao


login::[(String,String)]->[(String,String)]->IO (String,[(String,String)],[(String,String)])
login params session = do
			let login = param session "login"
			return (if login/="" then ("SUCCESS",("message",("already logged on with username-"++login)):[],[]) else ("SUCCESS",[],[]))

validate::[(String,String)]->[(String,String)]->IO (String,[(String,String)],[(String,String)])
validate params session = do
			let login=param params "login"
			let pwd=param params "pwd"
			val<-UserDao.validate login pwd
			return (if val then ("SUCCESS",[], (updateVal ("login",login) session)) else ("ERROR",("errormessage","Invalid credentials"):[],[]))
