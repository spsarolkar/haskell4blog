module Home
	where
import Blog
import BlogDao
import Constants

execute ::[(String,String)]-> [(String,String)] -> IO (String,[(String,String)],[(String,String)])
execute params sdata= do
		((Blog id title desc date),_) <-BlogDao.latest
		return ("SUCCESS",[("received",show params),("url",(hostname ++ "/blog/get/"++ id)),("test","test success")],sdata)

