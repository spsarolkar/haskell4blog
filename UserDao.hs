module UserDao
	where

import Database.SednaTypes
import Database.SednaBindings
import Prelude hiding (catch)
import Control.Exception
import Database.SednaBindings
import Database.Internal.SednaConnectionAttributes
import Text.XML.HXT.Core
import Data.Text.Internal
import qualified Data.Text as T
--import Data.ByteString.Char8   (pack, unpack)
import Data.Text
import Database.SednaExceptions
import Database.Internal.SednaConnectionAttributes
--
dbconnect = do
	let url="localhost"
	let dbname="user"
	let login="SYSTEM"
	let password="MANAGER"
	sednaConnect url dbname login password

data User = User String String String String
		deriving (Show)

validate::String -> String -> IO Bool
validate username password = do
				sednaCon <- dbconnect
				putStrLn "connected"
				catch (sednaBegin sednaCon) (\(e::SednaException) -> putStrLn $ show e)
				let validateQuery = "doc(\"user\")/users/user[name=\""++username++"\" and password=\""++password++"\"]"
				putStrLn ("Executing:" ++ validateQuery)
				catch (sednaExecute sednaCon (validateQuery)) (\(e::SednaException) -> putStrLn $ ((show e) ++ ("-----Error sednaExecute")))
				queryResult <- sednaGetResult sednaCon
				putStrLn ("Query Result:" ++ (unpack queryResult))
				catch (sednaCommit sednaCon) (\(e::SednaException) -> putStrLn $ "Error sednaCommit")
				catch (sednaCloseConnection sednaCon) (\(e::SednaException) -> putStrLn $ "Error sednaCloseConnection")
				return (if (unpack queryResult) == "" then False else True)
